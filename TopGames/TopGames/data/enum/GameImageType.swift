//
//  GameImageType.swift
//  TopGames
//
//  Created by Fagron Technologies on 04/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation

enum GameImageType: Int {
    case box, logo
}

enum GameImageSize: Int {
    case large, medium, small, template
}
