//
//  ViewController.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {
    
    private var games: [GameModel.Game] = []
    private var viewModel: MainViewModel!
    weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.activityIndicator = activityIndicatorView
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44)
        self.activityIndicator.startAnimating()
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.tableFooterView = self.activityIndicator

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.viewModel = MainViewModelFactory.create(appDelegate: appDelegate)
        
        self.getGames()
        
        refreshControl?.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "game", for: indexPath) as! GameCell
        
        let game = self.games[indexPath.row] as GameModel.Game
        cell.setModel(game: game, rank: indexPath.row + 1)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.games.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let game = self.games[indexPath.row]
        self.performSegue(withIdentifier: "gameDetail", sender: game)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.games.count - 1 {
            self.getGames()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gameDetail" {
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.game = sender as! GameModel.Game
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getGames(clearCache: true)
    }
    
    private func getGames (clearCache: Bool = false) {
        if self.viewModel.ended && !clearCache {
            self.tableView.tableFooterView = nil
        } else if !self.viewModel.loading {
            self.viewModel.listGames(clearCache: clearCache)
                .subscribe(onNext: { games in
                    if (clearCache) {
                        self.games = games
                        self.refreshControl?.endRefreshing()
                    } else {
                        self.games.append(contentsOf: games)
                    }
                    self.tableView.separatorStyle = .singleLine
                    self.tableView.reloadData()
                })
        }
    }
    
}

