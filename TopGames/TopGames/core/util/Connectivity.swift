//
//  Connectivity.swift
//  TopGames
//
//  Created by Fagron Technologies on 04/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    static func isConnectedToInternet () -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
